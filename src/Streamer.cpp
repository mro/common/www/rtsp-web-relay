/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:12:16
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/
#include "Streamer.hpp"

#include <chrono>
#include <exception>
#include <iostream>
#include <mutex>
#include <stdexcept>
#include <thread>
#include <vector>

#include <logger/Logger.hpp>

extern "C" {
#include <libavformat/avio.h>
}

#include "Consumer.hpp"

#define BUFFER_COUNT 20
#define BUFFER_SIZE (1024 * 1024)

static const std::string s_logCat{"rtsp:streamer"};
static const std::chrono::milliseconds s_reconnectDelay{10000};

const std::chrono::milliseconds Streamer::defaultUnsubscribeTimeout{30000};

const logger::Logger &operator<<(const logger::Logger &logger,
                                 AVIODataMarkerType marker)
{
    if (logger.isEnabled())
    {
        switch (marker)
        {
        case AVIO_DATA_MARKER_HEADER: logger << "header"; break;
        case AVIO_DATA_MARKER_SYNC_POINT: logger << "sync-point"; break;
        case AVIO_DATA_MARKER_BOUNDARY_POINT: logger << "boundary-point"; break;

        case AVIO_DATA_MARKER_UNKNOWN: logger << "unknown"; break;
        case AVIO_DATA_MARKER_TRAILER: logger << "trailer"; break;
        case AVIO_DATA_MARKER_FLUSH_POINT: logger << "flush-point"; break;
        default: logger << "invalid"; break;
        }
    }
    return logger;
}

Streamer::Streamer(const std::string &url,
                   const std::chrono::milliseconds &unsubscribeTimeout) :
    Thread("Streamer"),
    m_url(url),
    m_unsubscribeTimeout(unsubscribeTimeout),
    m_pool(BUFFER_COUNT, BUFFER_SIZE),
    m_streaming(false)
{}

Streamer::~Streamer()
{
    stop();
    release();
}

void Streamer::release()
{
    if (m_inputCtx)
        avformat_close_input(&m_inputCtx);
    /* close output */
    if (m_outputCtx)
    {
        if (m_outputCtx->pb)
            avio_context_free(&m_outputCtx->pb);
        avformat_free_context(m_outputCtx);
        m_outputCtx = nullptr;
    }
    m_header.reset();
    m_streamIndexMap.clear();
    m_buffer.reset();
}

int Streamer::write_data_type(void *opaque,
                              uint8_t *buf,
                              int buf_size,
                              enum AVIODataMarkerType type,
                              int64_t time)
{
    Streamer *self = static_cast<Streamer *>(opaque);

    if (!self->m_buffer)
    {
        logger::error(s_logCat) << "Internal error, no buffer set";
        return 0; // FIXME
    }

    Buffer &buffer = *self->m_buffer;
    buffer.type = type;
    buffer.beginOffset = buf - buffer.data();
    buffer.endOffset = buffer.beginOffset + buf_size;
    buffer.time = time;
    if (buffer.beginOffset > buffer.capacity())
    {
        logger::crit(s_logCat) << "Buffer corruption";
        return 0; // FIXME
    }

    logger::info(s_logCat) << "packet{" << type << "} size=" << buf_size;
    return self->onPacket();
}

int Streamer::onPacket()
{
    Buffer &packet = *m_buffer;
    if (packet.type == AVIO_DATA_MARKER_HEADER)
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if (m_header)
        {
            if ((m_header.use_count() > 1) ||
                (m_header->capacity() < packet.size() + m_header->size()))
            {
                const size_t bufSize = std::max(
                    m_header->capacity(), packet.size() + m_header->size());

                /* aggregate header will be out of pool */
                Buffer *header = new Buffer(bufSize, nullptr);
                header->type = m_header->type;
                header->time = m_header->time;
                header->append(m_header->begin(), m_header->end());
                header->append(packet.begin(), packet.end());

                logger::debug(s_logCat)
                    << "header re-allocated, new size: " << header->size();
                m_header.reset(header);
            }
            else
            {
                m_header->append(packet.begin(), packet.end());
            }
        }
        else
        {
            m_header = m_buffer;
        }
    }

    {
        std::list<Consumer::Id> removeList;
        std::lock_guard<std::mutex> lock(m_lock);
        for (auto &it : m_consumers)
        {
            Consumer::Shared consumer{it.second.lock()};
            if (consumer)
                consumer->push(m_buffer);
            else
                removeList.push_back(it.first);
        }
        for (Consumer::Id id : removeList)
        {
            // consumers should un-subscribe themselves
            logger::warning(s_logCat) << "removing dead consumer:" << id;
            m_consumers.erase(id);
        }
    }
    // {
    //     FILE *fd = fopen("test_header_dump.mp4", "a");
    //     fwrite(&buffer.data()[buffer.start], 1, buffer.end - buffer.start,
    //            fd);
    //     fclose(fd);
    // }

    // setup new buffer
    Buffer::Shared buffer = m_pool.take();
    if (!buffer)
        logger::crit(s_logCat) << "failed to allocate buffer";
    else
    {
        m_buffer = buffer;
        AVIOContext *out = m_outputCtx->pb;
        out->buf_ptr = out->buf_ptr_max = out->buffer = buffer->data();
        out->buffer_size = buffer->capacity();
        out->buf_end = out->buffer + out->buffer_size;
    }
    return 0;
}

void Streamer::subscribe(const Consumer::Shared &consumer)
{
    if (!consumer)
        return;

    /* one is reserved for headers, another to always have one free */
    consumer->initialize(m_pool.capacity() - 2);
    bool streaming = false;
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_consumers[consumer->id] = consumer;

        if (m_header)
            consumer->push(m_header);

        streaming = m_streaming;
    }

    if (!streaming)
        wake();
}

void Streamer::unsubscribe(Consumer::Id id)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_consumers.erase(id);
}

void Streamer::thread_func()
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if (m_consumers.empty())
        {
            logger::info(s_logCat) << "no consumers, going idle";
            return;
        }
        m_streaming = true;
        logger::notice(s_logCat) << "starting stream: " << m_url;
    }

    bool hasError = false;
    try
    {
        int ret;
        ret = avformat_open_input(&m_inputCtx, m_url.c_str(), NULL, NULL);
        if (ret < 0)
            throw std::runtime_error(std::string("Error occurred: ") +
                                     avpp_err2str(ret));

        ret = avformat_find_stream_info(m_inputCtx, NULL);
        if (ret < 0)
            throw std::runtime_error(std::string("Error occurred: ") +
                                     avpp_err2str(ret));

        ret = avformat_alloc_output_context2(&m_outputCtx, NULL, "mp4", NULL);
        if (ret < 0)
            throw std::runtime_error(std::string("Error occurred: ") +
                                     avpp_err2str(ret));

        size_t streams = 0;
        for (size_t i = 0; i < m_inputCtx->nb_streams; ++i)
        {
            AVStream *stream = m_inputCtx->streams[i];
            switch (stream->codecpar->codec_type)
            {
            case AVMEDIA_TYPE_VIDEO: {
                m_streamIndexMap[i] = streams++;
                AVStream *outStream = avformat_new_stream(m_outputCtx, NULL);
                if (!outStream)
                    throw std::runtime_error("Failed to allocate out stream");
                ret = avcodec_parameters_copy(outStream->codecpar,
                                              stream->codecpar);
                if (ret < 0)
                    throw std::runtime_error(
                        std::string("Failed to copy codec parameters: ") +
                        avpp_err2str(ret));

                // set stream codec tag to 0, for libav to detect automatically
                outStream->codecpar->codec_tag = 0;
            }
            break;
            case AVMEDIA_TYPE_AUDIO:
            case AVMEDIA_TYPE_SUBTITLE:
            default: break;
            }
        }
        if (streams == 0)
            throw std::runtime_error("No video streams found");

        logger::info(s_logCat) << "stream found";
        av_dump_format(m_outputCtx, 0, NULL, 1);

        m_buffer = m_pool.take();
        if (!m_buffer)
            throw std::runtime_error("Failed to take initial buffer");

        m_outputCtx->pb = avio_alloc_context(
            m_buffer->data(), m_buffer->capacity(), 1, this, NULL, NULL, NULL);
        m_outputCtx->pb->write_data_type = Streamer::write_data_type;

        AVDictionary *opts = NULL;
        // https://developer.mozilla.org/en-US/docs/Web/API/Media_Source_Extensions_API/Transcoding_assets_for_MSE
        av_dict_set(&opts, "movflags",
                    "frag_keyframe+empty_moov+default_base_moof+faststart", 0);
        // av_dict_set(&opts, "frag_duration", "100", 0);

        ret = avformat_write_header(m_outputCtx, &opts);
        if (ret < 0)
            throw std::runtime_error(std::string("Failed to write header: ") +
                                     avpp_err2str(ret));

        logger::info(s_logCat) << "streaming";
        std::chrono::steady_clock::time_point lastConsumed{
            std::chrono::steady_clock::now()};
        while (m_started.load())
        {
            AVPacket packet;
            AVStream *in_stream, *out_stream;
            ret = av_read_frame(m_inputCtx, &packet);
            if (ret == AVERROR_EOF)
            {
                logger::info(s_logCat) << "end of stream";
                break;
            }
            else if (ret < 0)
            {
                logger::warning(s_logCat)
                    << "av_read_frame failed: " << avpp_err2str(ret)
                    << std::endl;
                hasError = true;
                break;
            }

            in_stream = m_inputCtx->streams[packet.stream_index];
            std::map<size_t, size_t>::const_iterator it = m_streamIndexMap.find(
                packet.stream_index);
            if (it == m_streamIndexMap.cend())
            {
                av_packet_unref(&packet);
                continue;
            }
            packet.stream_index = it->second;
            out_stream = m_outputCtx->streams[packet.stream_index];

            /* copy packet */
            packet.pts = av_rescale_q_rnd(
                packet.pts, in_stream->time_base, out_stream->time_base,
                (AVRounding) (AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.dts = av_rescale_q_rnd(
                packet.dts, in_stream->time_base, out_stream->time_base,
                (AVRounding) (AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.duration = av_rescale_q(
                packet.duration, in_stream->time_base, out_stream->time_base);
            // https://ffmpeg.org/doxygen/trunk/structAVPacket.html#ab5793d8195cf4789dfb3913b7a693903
            packet.pos = -1;

            // https://ffmpeg.org/doxygen/trunk/group__lavf__encoding.html#ga37352ed2c63493c38219d935e71db6c1
            ret = av_interleaved_write_frame(m_outputCtx, &packet);
            if (ret < 0)
            {
                logger::warning(s_logCat)
                    << "Error muxing packet: " << avpp_err2str(ret);
            }
            av_packet_unref(&packet);

            {
                std::lock_guard<std::mutex> lock{m_lock};
                if (m_consumers.empty())
                {
                    if (std::chrono::steady_clock::now() >=
                        lastConsumed + m_unsubscribeTimeout)
                    {
                        logger::notice(s_logCat)
                            << "stream idle for more than "
                            << m_unsubscribeTimeout << ", closing";
                        break;
                    }
                }
                else
                    lastConsumed = std::chrono::steady_clock::now();
            }
        }
        // https://ffmpeg.org/doxygen/trunk/group__lavf__encoding.html#ga7f14007e7dc8f481f054b21614dfec13
        av_write_trailer(m_outputCtx);
    }
    catch (std::exception &ex)
    {
        logger::warning(s_logCat) << ex.what();
        hasError = true;
    }
    release();

    {
        /* warn consumers about end of stream and kick them out */
        m_buffer.reset();
        Buffer::Shared buffer = m_pool.take();
        if (!buffer)
        {
            logger::crit(s_logCat) << "failed to allocate buffer";
            std::lock_guard<std::mutex> lock{m_lock};
            m_streaming = false;
        }
        else
        {
            buffer->type = AVIO_DATA_MARKER_TRAILER;
            buffer->beginOffset = 0;
            buffer->endOffset = 0;

            std::map<Consumer::Id, std::weak_ptr<Consumer>> consumers;

            {
                std::lock_guard<std::mutex> lock{m_lock};
                m_streaming = false;
                consumers.swap(m_consumers);
            }

            for (auto &it : consumers)
            {
                Consumer::Shared consumer{it.second.lock()};
                if (consumer)
                {
                    consumer->push(buffer);
                    consumer->unsubscribe();
                }
            }
        }
        if (hasError)
        {
            /* wait a before before trying to reconnect */
            logger::warning(s_logCat)
                << "stream had errors, adding re-connection delay";
            std::this_thread::sleep_for(s_reconnectDelay);
        }
    }
}
