/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-23T20:06:18
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef STREAMERFACTORY_HPP__
#define STREAMERFACTORY_HPP__

#include <map>
#include <mutex>
#include <string>

#include "Streamer.hpp"

class StreamerFactory
{
public:
    ~StreamerFactory();

    void load(const std::string &config);

    inline std::shared_ptr<Streamer> get(const std::string &key) const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        auto it{m_streamers.find(key)};
        return (it != m_streamers.end()) ? it->second :
                                           std::shared_ptr<Streamer>();
    }

    inline size_t size() const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return m_streamers.size();
    }

    inline std::string getConfig() const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return m_config;
    }

protected:
    mutable std::mutex m_lock;
    std::map<std::string, std::shared_ptr<Streamer>> m_streamers;
    std::string m_config;
};

#endif