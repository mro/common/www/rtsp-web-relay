/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:12:08
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <chrono>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <memory>

#include <logger/Logger.hpp>

#include "Consumer.hpp"
#include "Streamer.hpp"
#include "StreamerFactory.hpp"
#include "server/Server.hpp"

static const std::string s_logCat{"rtsp:main"};
static const std::vector<std::string> s_configFiles{"/etc/rtsp-web-relay.yaml",
                                                    "rtsp-web-relay.yaml"};

std::string readAll(const std::string &filename)
{
    std::ifstream ifs{filename};
    return std::string{std::istreambuf_iterator<char>{ifs},
                       std::istreambuf_iterator<char>{}};
}

int main(int argc, char **argv)
{
    // av_log_set_level(AV_LOG_DEBUG);

    // Server server;
    // Streamer streamer{"rtsp://127.0.0.1:8554/",
    // std::chrono::milliseconds::zero()}; Consumer::Shared consumer{new
    // Consumer()}; streamer.subscribe(consumer);

    // server.run();

    std::shared_ptr<StreamerFactory> factory{new StreamerFactory{}};

    if (argc > 1)
    {
        for (int i = 1; i < argc; ++i)
            factory->load(readAll(argv[i]));
    }
    else
    {
        for (const std::string &filename : s_configFiles)
        {
            const std::string content = readAll(filename);
            if (!content.empty())
                factory->load(content);
        }
    }
    logger::notice(s_logCat) << "loaded streams: " << factory->size();

    Server server;
    server.connect(factory);

    server.run();

    // try
    // {
    //     streamer.run();
    // }
    // catch (std::exception &ex)
    // {
    //     std::cerr << ex.what() << std::endl;
    // }
    return 0;
}
