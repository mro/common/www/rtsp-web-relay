/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-14T14:48:00
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Environment.hpp"

#include <logger/Logger.hpp>
#include <oatpp/core/base/Environment.hpp>

#include <ccut/Singleton.hxx>

template class ccut::Singleton<Environment, ccut::SingletonType::automatic>;

class Logger : public oatpp::base::Logger
{
public:
    void log(v_uint32 priority,
             const std::string &tag,
             const std::string &message)
    {
        logger::log(loggerLevel(priority), "http:" + tag) << message;
    }

    bool isLogPriorityEnabled(v_uint32 prio)
    {
        return loggerLevel(prio) <= logger::Config().getLevel();
    }

    inline logger::Level loggerLevel(v_uint32 prio) const
    {
        switch (prio)
        {
        case PRIORITY_V:
        case PRIORITY_D: return logger::Level::DEBUG;
        case PRIORITY_I: return logger::Level::NOTICE;
        case PRIORITY_W: return logger::Level::WARNING;
        case PRIORITY_E: return logger::Level::ERROR;
        default: return logger::Level::INFO;
        }
    }
};

Environment::Environment() : m_logger(new Logger)
{
    oatpp::base::Environment::init();
    oatpp::base::Environment::setLogger(m_logger);
    oatpp::base::Environment::printCompilationConfig();
}

Environment::~Environment()
{
    oatpp::base::Environment::destroy();
}
