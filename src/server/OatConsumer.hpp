/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T23:06:40
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef OATCONSUMER_HPP__
#define OATCONSUMER_HPP__

#include <oatpp/web/protocol/http/outgoing/StreamingBody.hpp>

#include "Consumer.hpp"
#include "Streamer.hpp"

class OatConsumer : public Consumer, public oatpp::data::stream::ReadCallback
{
public:
    virtual ~OatConsumer();

    oatpp::v_io_size read(void *buffer,
                          v_buff_size count,
                          oatpp::async::Action &) override;

    Buffer::Shared m_current;
    size_t m_pos;
};

#endif