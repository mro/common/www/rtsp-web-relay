/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-14T14:46:33
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef ENVIRONMENT_HPP__
#define ENVIRONMENT_HPP__

#include <memory>

#include <ccut/Singleton.hpp>

namespace oatpp {
namespace base {
class Logger;
} // namespace base
} // namespace oatpp

class Environment :
    public ccut::Singleton<Environment, ccut::SingletonType::automatic>
{
public:
    ~Environment();

protected:
    Environment();
    std::shared_ptr<oatpp::base::Logger> m_logger;

    friend class ccut::Singleton<Environment, ccut::SingletonType::automatic>;
};

#endif