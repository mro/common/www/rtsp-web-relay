/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-11T13:35:13
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef APICONTROLLER_HPP__
#define APICONTROLLER_HPP__

#include <algorithm>
#include <memory>

#include <ccut/async.hpp>
#include <ccut/utils.hpp>
#include <logger/Logger.hpp>
#include <oatpp/core/data/stream/Stream.hpp>
#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/macro/component.hpp>
#include <oatpp/web/server/api/ApiController.hpp>

#include "OatConsumer.hpp"
#include "StreamerFactory.hpp"

#include OATPP_CODEGEN_BEGIN(ApiController)

class ApiController : public oatpp::web::server::api::ApiController
{
public:
    explicit ApiController(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>,
                                           objectMapper)) :
        oatpp::web::server::api::ApiController(objectMapper)
    {}

    ENDPOINT_INFO(root)
    {
        info->summary = "api-docs redirection";
        info->addResponse<List<String>>(Status::CODE_302, "");
    }
    ENDPOINT("GET", "/", root)
    {
        auto ret = createResponse(Status::CODE_302);
        ret->putHeader("Location", "/api-docs/ui");
        return ret;
    }

    ENDPOINT("GET", "/index.html", index);

    ENDPOINT_INFO(config)
    {
        info->summary = "get config document";
        info->addResponse<List<String>>(Status::CODE_200, "application/json");
    }
    ENDPOINT("GET", "/config", config)
    {
        std::shared_ptr<StreamerFactory> factory{m_streamerFactory.lock()};
        if (!factory)
            return createResponse(Status::CODE_500, "no streamer factory");

        auto ret = createResponse(Status::CODE_200, factory->getConfig());
        ret->putHeader(oatpp::web::protocol::http::Header::CONTENT_TYPE,
                       oatpp::web::protocol::http::Header::Value::
                           CONTENT_TYPE_APPLICATION_JSON);
        return ret;
    }

    ENDPOINT("GET",
             "/stream/{group}/{name}",
             stream,
             PATH(String, group),
             PATH(String, name))
    {
        std::shared_ptr<StreamerFactory> factory{m_streamerFactory.lock()};
        if (!factory)
            return createResponse(Status::CODE_500, "no streamer factory");

        const std::string streamName{ccut::urldecode(group + "/" + name)};
        std::shared_ptr<Streamer> streamer{factory->get(streamName)};
        if (!streamer)
            return createResponse(Status::CODE_500,
                                  "no such stream: " + streamName);

        std::shared_ptr<OatConsumer> consumer{new OatConsumer{}};
        consumer->subscribe(streamer);

        auto body =
            std::make_shared<oatpp::web::protocol::http::outgoing::StreamingBody>(
                consumer);
        auto ret = OutgoingResponse::createShared(Status::CODE_200, body);
        ret->putHeader("Content-Type", "video/mp4");

        if (!streamer->isStarted())
            streamer->start();

        return ret;
    }

    std::weak_ptr<StreamerFactory> m_streamerFactory;
};

#include OATPP_CODEGEN_END(ApiController)

#endif