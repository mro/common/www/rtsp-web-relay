/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-11T13:24:55
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Server.hpp"

#include <cstdlib>
#include <string>

#include <oatpp/web/server/handler/ErrorHandler.hpp>
#include <oatpp/web/server/interceptor/RequestInterceptor.hpp>
#include <unistd.h>

#define SWAGGER_ROOT_PATH "/api-docs"
#define SWAGGER_UI_PATH "/ui"

#include <ccut/Thread.hpp>
#include <oatpp-swagger/Controller.hpp>
#include <oatpp-swagger/Model.hpp>
#include <oatpp-swagger/Resources.hpp>
#include <oatpp/core/macro/component.hpp>
#include <oatpp/network/Server.hpp>
#include <oatpp/network/tcp/server/ConnectionProvider.hpp>
#include <oatpp/parser/json/mapping/ObjectMapper.hpp>
#include <oatpp/web/server/HttpConnectionHandler.hpp>

#include "ApiController.hpp"
#include "Environment.hpp"

/* swagger generated resources */
#include "favicon-16x16.png.cpp"
#include "favicon-32x32.png.cpp"
#include "index.html.cpp"
#include "oauth2-redirect.html.cpp"
#include "swagger-ui-bundle.js.cpp"
#include "swagger-ui-es-bundle-core.js.cpp"
#include "swagger-ui-es-bundle.js.cpp"
#include "swagger-ui-standalone-preset.js.cpp"
#include "swagger-ui.css.cpp"
#include "swagger-ui.js.cpp"

#define ENV_PORT "DEBUG_PORT"
#define ENV_ADDR "DEBUG_ADDR"
// used by swagger
#define ENV_HOSTNAME "DEBUG_HOSTNAME"

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 128
#endif

namespace http = oatpp::web::protocol::http;

class RequestInterceptor :
    public oatpp::web::server::interceptor::RequestInterceptor
{
public:
    std::shared_ptr<OutgoingResponse> intercept(
        const std::shared_ptr<IncomingRequest> &request) override
    {
        const auto &line = request->getStartingLine();
        OATPP_LOGV("req", "%s %s", line.method.std_str().c_str(),
                   line.path.std_str().c_str());

        if (line.method == "OPTIONS")
            return OutgoingResponse::createShared(http::Status::CODE_204,
                                                  nullptr);

        return nullptr;
    }
};

class ResponseInterceptor :
    public oatpp::web::server::interceptor::ResponseInterceptor
{
public:
    std::shared_ptr<OutgoingResponse> intercept(
        const std::shared_ptr<IncomingRequest> &request,
        const std::shared_ptr<OutgoingResponse> &response) override
    {
        response->putOrReplaceHeader(http::Header::SERVER, "CERN/MRO");
        const auto &line = request->getStartingLine();
        const int32_t code = response->getStatus().code;
        if (code >= 400)
        {
            OATPP_LOGE("req", "%s %s -> %i", line.method.std_str().c_str(),
                       line.path.std_str().c_str(), code);
        }
        else
        {
            OATPP_LOGV("req", "%s %s -> %i", line.method.std_str().c_str(),
                       line.path.std_str().c_str(), code);
        }

        response->putHeaderIfNotExists(http::Header::CORS_ORIGIN, "*");
        response->putHeaderIfNotExists(http::Header::CORS_HEADERS, "*");
        if (line.method == "OPTIONS")
        {
            response->putHeaderIfNotExists(http::Header::CORS_METHODS,
                                           "GET,PUT,POST,DELETE,OPTIONS");
            response->putHeaderIfNotExists(http::Header::CORS_MAX_AGE, "172800");
        }
        return response;
    }
};

class ErrorHandler : public oatpp::web::server::handler::DefaultErrorHandler
{
public:
    using oatpp::web::server::handler::DefaultErrorHandler::handleError;

    std::shared_ptr<http::outgoing::Response> handleError(
        const http::Status &status,
        const oatpp::String &message,
        const Headers &headers) override
    {
        OATPP_LOGE("req", "error: %s", message.getValue("").c_str());
        return DefaultErrorHandler::handleError(status, message, headers);
    }
};

class ServerData : public ccut::Thread
{
public:
    static const oatpp::network::Address &getAddress()
    {
        const char *addr = getenv(ENV_ADDR);
        const char *port_str = getenv(ENV_PORT);
        uint16_t port = (port_str) ? std::stoul(std::string(port_str)) : 8080;

        static const oatpp::network::Address address = {
            (addr ? addr : "0.0.0.0"), port, oatpp::network::Address::IP_4};
        return address;
    }

    /**
     *  Create ConnectionProvider component which listens on the port
     */
    OATPP_CREATE_COMPONENT(
        std::shared_ptr<oatpp::network::ServerConnectionProvider>,
        serverConnectionProvider)
    ([] {
        return oatpp::network::tcp::server::ConnectionProvider::createShared(
            ServerData::getAddress());
    }());

    /**
     *  Create Router component
     */
    OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>,
                           httpRouter)
    ([] { return oatpp::web::server::HttpRouter::createShared(); }());

    /**
     *  Create ConnectionHandler component which uses Router component to route
     * requests
     */
    OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,
                           serverConnectionHandler)
    ([] {
        OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>,
                        router); // get Router component
        auto ret = oatpp::web::server::HttpConnectionHandler::createShared(
            router);
        ret->addRequestInterceptor(std::make_shared<RequestInterceptor>());
        ret->addResponseInterceptor(std::make_shared<ResponseInterceptor>());
        return ret;
    }());

    /**
     *  Create ObjectMapper component to serialize/deserialize DTOs in
     * Contoller's API
     */
    OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>,
                           apiObjectMapper)
    ([] {
        return oatpp::parser::json::mapping::ObjectMapper::createShared();
    }());

    OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::swagger::DocumentInfo>,
                           swaggerDocumentInfo)
    ([] {
        oatpp::swagger::DocumentInfo::Builder builder;

        builder.setTitle("Motion-Library")
            .setDescription("Motion Library debug interface")
            .setVersion("1.0");

        const std::string port = std::to_string(ServerData::getAddress().port);
        {
            const char *hostname = getenv(ENV_HOSTNAME);
            if (hostname)
            {
                builder.addServer(
                    std::string("http://") + hostname + std::string(":") + port,
                    "configured server hostname");
            }
        }

        {
            char hostname[HOST_NAME_MAX + 1];
            if (gethostname(hostname, HOST_NAME_MAX) == 0)
            {
                builder.addServer(
                    std::string("http://") + hostname + std::string(":") + port,
                    "detected server hostname");
            }
        }
        builder.addServer("http://localhost:" + port, "server on localhost");
        return builder.build();
    }());

    /**
     *  Swagger-Ui Resources (<oatpp-examples>/lib/oatpp-swagger/res)
     */
    OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::swagger::Resources>,
                           swaggerResources)
    ([] {
        auto res = std::make_shared<oatpp::swagger::Resources>("/tmp");
        res->cacheResource("favicon-16x16.png", favicon_16x16_png);
        res->cacheResource("favicon-32x32.png", favicon_32x32_png);
        res->cacheResource("index.html", index_html);
        res->cacheResource("oauth2-redirect.html", oauth2_redirect_html);
        res->cacheResource("swagger-ui-bundle.js", swagger_ui_bundle_js);
        res->cacheResource("swagger-ui-bundle.js.map", "");
        res->cacheResource("swagger-ui-es-bundle-core.js",
                           swagger_ui_es_bundle_core_js);
        res->cacheResource("swagger-ui-es-bundle-core.js.map", "");
        res->cacheResource("swagger-ui-es-bundle.js", swagger_ui_es_bundle_js);
        res->cacheResource("swagger-ui-es-bundle.js.map", "");
        res->cacheResource("swagger-ui-standalone-preset.js",
                           swagger_ui_standalone_preset_js);
        res->cacheResource("swagger-ui-standalone-preset.js.map", "");
        res->cacheResource("swagger-ui.css", swagger_ui_css);
        res->cacheResource("swagger-ui.css.map", "");
        res->cacheResource("swagger-ui.js", swagger_ui_js);
        res->cacheResource("swagger-ui.js.map", "");

        return res;
    }());

    void stop()
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_started.store(false);
            m_waked = true;
            m_cond.notify_all();
        }

        server->stop();

        OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,
                        serverConnectionHandler);
        serverConnectionHandler->stop();

        Thread::stop();
    }

    virtual void thread_func() override;

    std::shared_ptr<ApiController> apiController;
    std::shared_ptr<oatpp::swagger::Controller> swaggerController;
    std::unique_ptr<oatpp::network::Server> server;
};

void ServerData::thread_func()
{
    {
        OATPP_COMPONENT(
            std::shared_ptr<oatpp::network::ServerConnectionProvider>,
            connectionProvider);
        OATPP_LOGI("oatpp", "Server running on port %s",
                   connectionProvider->getProperty("port").getData());
    }
    server->run();
}

Server::Server()
{
    init();
}

Server::~Server()
{
    stop();
}

void Server::connect(const std::shared_ptr<StreamerFactory> &factory)
{
    m_data->apiController->m_streamerFactory = factory;
}

void Server::stop()
{
    if (m_data)
        m_data->stop();
    m_data.reset();
    m_env.reset(); /* env must be released last */
}

void Server::start()
{
    init();
    if (m_data)
        m_data->start();
}

void Server::run()
{
    init();
    if (m_data)
        m_data->run();
}

void Server::init()
{
    if (m_data)
        return;

    if (!m_env)
        m_env = Environment::instance();
    m_data.reset(new ServerData);

    OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router);

    m_data->apiController.reset(new ApiController);

    std::shared_ptr<oatpp::web::server::handler::ErrorHandler> errHdlr{
        new ErrorHandler};
    oatpp::web::server::api::Endpoints ep;
    m_data->apiController->setErrorHandler(errHdlr);
    router->addController(m_data->apiController);
    ep.append(m_data->apiController->getEndpoints());

    m_data->swaggerController = oatpp::swagger::Controller::createShared(ep);
    router->addController(m_data->swaggerController);

    /* Get connection handler component */
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,
                    connectionHandler);

    /* Get connection provider component */
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>,
                    connectionProvider);

    /* Create server which takes provided TCP connections and passes them to
     * HTTP connection handler */
    m_data->server.reset(
        new oatpp::network::Server(connectionProvider, connectionHandler));
}
