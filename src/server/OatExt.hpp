/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-14T19:40:33
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef OATEXT_HPP__
#define OATEXT_HPP__

#include <string>
#include <type_traits>

#include <logger/Logger.hpp>
#include <oatpp/core/Types.hpp>
#include <oatpp/web/server/api/ApiController.hpp>

namespace logger {

template<typename T>
using is_oatpp_List =
    std::is_same<T, oatpp::List<typename T::TemplateObjectType::value_type>>;

template<typename T>
using is_oatpp_UnorderedSet =
    std::is_same<T,
                 oatpp::UnorderedSet<typename T::TemplateObjectType::value_type>>;

/* disable logger implementations g++ 4.8.5 workarround */
template<typename T>
struct exclude_default<T, typename is_oatpp_List<T>::type> :
    public std::true_type
{};
template<typename T>
struct exclude_default<T, typename is_oatpp_UnorderedSet<T>::type> :
    public std::true_type
{};

template<typename T>
typename std::enable_if<is_oatpp_List<T>::value || is_oatpp_UnorderedSet<T>::value,
                        const Logger &>::type
    operator<<(const Logger &logger, const T &value)
{
    if (logger.isEnabled())
        logger << (value ? *value : typename T::TemplateObjectType{});
    return logger;
}

inline const Logger &operator<<(const Logger &logger,
                                const oatpp::data::mapping::type::String &value)
{
    if (logger.isEnabled())
        logger << (value ? *value : "nullptr");
    return logger;
}

} // namespace logger

void addTag(oatpp::web::server::api::ApiController &ctrl,
            const std::string &tag);

#endif
