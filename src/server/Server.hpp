/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-08T14:13:17
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef SERVER_HPP__
#define SERVER_HPP__

#include <memory>

#include "StreamerFactory.hpp"

class ServerData;
class Environment;

class Server
{
public:
    Server();
    ~Server();

    /**
     * @brief (re)connect the debug server
     */
    void connect(const std::shared_ptr<StreamerFactory> &factory);

    /**
     * @brief disconnect the debug server
     */
    void disconnect();

    void start();
    void stop();
    void run();

protected:
    void init();

    std::unique_ptr<ServerData> m_data;
    std::shared_ptr<Environment> m_env;
};

#endif