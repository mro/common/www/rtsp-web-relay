/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-23T22:26:32
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "OatConsumer.hpp"

#include <stdexcept>

#include <libavformat/avio.h>
#include <logger/Logger.hpp>

static const std::string s_logCat{"rtsp:consumer:oatpp"};

OatConsumer::~OatConsumer()
{
    logger::info(s_logCat) << "releasing consumer";
    unsubscribe();
}

oatpp::v_io_size OatConsumer::read(void *buffer,
                                   v_buff_size count,
                                   oatpp::async::Action &)
{
    if (m_current && m_pos >= m_current->endOffset)
    {
        if (m_current->type == AVIO_DATA_MARKER_TRAILER)
        {
            logger::info(s_logCat) << "end of stream: trailer packet found";
            return 0;
        }
        m_current.reset();
    }

    if (!m_current)
    {
        try
        {
            m_current = m_packets->pop();
        }
        catch (ccut::Exception &ex)
        {
            logger::info(s_logCat)
                << "end of stream: interrupted: " << ex.what();
            return 0;
        }
        m_pos = m_current ? m_current->beginOffset : 0;
    }

    if (m_current)
    {
        count = std::min(count, v_buff_size(m_current->endOffset - m_pos));

        ::memcpy(buffer, &m_current->buf[m_pos], count);
        m_pos += count;
        return count;
    }
    else
    {
        logger::debug(s_logCat) << "end of stream: empty buffer";
        /* end of stream */
        return 0;
    }
}
