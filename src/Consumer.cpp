/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:12:00
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Consumer.hpp"

#include <algorithm>
#include <atomic>

#include <logger/Logger.hpp>

#include "Streamer.hpp"

static std::atomic<Consumer::Id> s_ids{0};
static const std::string s_logCat{"rtsp:consumer"};

Consumer::Consumer() noexcept : id{s_ids.fetch_add(1)}
{
    logger::info(s_logCat) << "consumer created id:" << id;
}

Consumer::~Consumer()
{
    logger::info(s_logCat) << "consumer released id:" << id;
    unsubscribe();
}

void Consumer::subscribe(std::shared_ptr<Streamer> &streamer)
{
    if (!streamer)
        unsubscribe();
    else if (!m_streamer.expired())
    {
        logger::crit(s_logCat)
            << "consumer already subscribed to another streamer";
    }
    else
    {
        m_streamer = streamer;
        streamer->subscribe(shared_from_this());
    }
}

void Consumer::unsubscribe()
{
    std::shared_ptr<Streamer> streamer{m_streamer.lock()};
    if (streamer)
    {
        streamer->unsubscribe(id);
        m_streamer.reset();
    }
}

void Consumer::initialize(size_t maxPackets)
{
    /* one buffer may be poped and used by Consumer */
    m_packets.reset(new ccut::Queue<Buffer::Shared>(
        "Consumer", std::max(maxPackets - 1, size_t{1})));
    m_state = WAIT_HEADER;
}

bool Consumer::push(Buffer::Shared &packet)
{
    if (!packet)
    {
        logger::warning(s_logCat) << "pushing a null packet";
        return false;
    }

    switch (m_state)
    {
    case NOT_INIT:
        logger::error(s_logCat)
            << "pushing packet on a non-initalized consumer";
        return false;
    case WAIT_HEADER:
        if (packet->type == AVIO_DATA_MARKER_TRAILER && enqueue(packet)) {}
        else if (packet->type == AVIO_DATA_MARKER_HEADER && enqueue(packet))
        {
            m_state = WAIT_SYNC;
            return true;
        }
        return false;
    case WAIT_SYNC:
        if (packet->type == AVIO_DATA_MARKER_TRAILER && enqueue(packet)) {}
        else if (packet->type == AVIO_DATA_MARKER_HEADER && enqueue(packet))
        { /* there may be several headers packets */
        }
        else if (packet->type == AVIO_DATA_MARKER_SYNC_POINT && enqueue(packet))
        {
            m_state = STREAMING;
            return true;
        }
        return false;
    case STREAMING: return enqueue(packet);
    default:
        logger::crit(s_logCat) << "invalid state for consumer: " << m_state;
        return false;
    }
}

bool Consumer::enqueue(Buffer::Shared &packet)
{
    try
    {
        m_packets->post(packet);
        logger::debug(s_logCat) << "buffer queued consumer:" << id
                                << " queued:" << m_packets->count();
        return true;
    }
    catch (ccut::Exception &ex)
    {
        logger::warning(s_logCat) << "consumer id:" << id << " stale, flushing";
        m_packets->clear();
        m_state = WAIT_SYNC;
        return false;
    }
}