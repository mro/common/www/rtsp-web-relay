/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-17T01:18:55
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef BUFFER_HPP__
#define BUFFER_HPP__

#include <algorithm>
#include <cstdint>
#include <deque>
#include <memory>
#include <mutex>
#include <vector>

extern "C" {
#include <libavformat/avio.h>
}

#include "avpp.hpp"

class BufferPool;

struct Buffer
{
    typedef std::shared_ptr<Buffer> Shared;
    typedef std::vector<uint8_t> Data;

    explicit Buffer(size_t size, BufferPool *parent = nullptr) :
        buf(size),
        beginOffset(0),
        endOffset(0),
        type(AVIO_DATA_MARKER_UNKNOWN),
        time(-1),
        parent(parent)
    {}

    inline size_t capacity() const { return buf.size(); }
    inline uint8_t *data() { return buf.data(); }

    template<typename Iterator>
    void append(Iterator first, Iterator last)
    {
        auto out = buf.begin() + endOffset;
        endOffset = std::copy(first, last, out) - buf.begin();
    }

    inline Data::iterator begin() { return buf.begin() + beginOffset; }
    inline Data::iterator end() { return buf.begin() + endOffset; }
    inline size_t size() const { return endOffset - beginOffset; }

    std::vector<uint8_t> buf;
    size_t beginOffset;
    size_t endOffset;

    AVIODataMarkerType type;
    int64_t time;

protected:
    friend class BufferPool;
    BufferPool *parent;
};

class BufferPool
{
public:
    BufferPool(size_t capacity, size_t bufferSize) :
        m_capacity{capacity}, m_bufferSize{bufferSize}
    {
        for (size_t i = 0; i < capacity; ++i)
            m_pool.emplace_back(new Buffer(bufferSize, this));
    }

    inline size_t capacity() { return m_capacity; }
    inline size_t bufferSize() { return m_bufferSize; }

    Buffer::Shared take()
    {
        std::lock_guard<std::mutex> l(m_lock);
        if (m_pool.empty())
            return Buffer::Shared();
        Buffer::Shared ret(m_pool.front().release(), BufferRelease());
        m_pool.pop_front();
        return ret;
    }

protected:
    struct BufferRelease
    {
        void operator()(Buffer *b)
        {
            if (b->parent)
            {
                std::lock_guard<std::mutex> l(b->parent->m_lock);
                b->parent->m_pool.emplace_back(b);
            }
            else
                delete b;
        }
    };

    const size_t m_capacity;
    const size_t m_bufferSize;
    std::mutex m_lock;
    std::deque<std::unique_ptr<Buffer>> m_pool;
};

#endif