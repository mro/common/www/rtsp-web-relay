/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:12:23
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef STREAMER_HPP__
#define STREAMER_HPP__

#include <chrono>
#include <deque>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include <ccut/Thread.hpp>

#include "Buffer.hpp"
#include "Consumer.hpp"
#include "avpp.hpp"

class Streamer : public ccut::Thread
{
public:
    /**
     * @brief Construct a new Streamer object
     *
     * @param url RTSP url to connect
     */
    explicit Streamer(const std::string &url,
                      const std::chrono::milliseconds &unsubscribeTimeout =
                          defaultUnsubscribeTimeout);
    ~Streamer();

    void subscribe(const Consumer::Shared &consumer);
    void unsubscribe(Consumer::Id consumer);

    inline bool isStarted() const { return m_started; }

    static const std::chrono::milliseconds defaultUnsubscribeTimeout;

protected:
    void thread_func() override;
    void release();

    static int write_data_type(void *opaque,
                               uint8_t *buf,
                               int buf_size,
                               enum AVIODataMarkerType type,
                               int64_t time);
    int onPacket();

    const std::string m_url;
    const std::chrono::milliseconds m_unsubscribeTimeout;
    AVFormatContext *m_inputCtx = NULL;
    AVFormatContext *m_outputCtx = NULL;
    std::map<size_t, size_t> m_streamIndexMap;
    BufferPool m_pool;
    Buffer::Shared m_buffer;

    std::mutex m_lock;
    std::map<Consumer::Id, std::weak_ptr<Consumer>> m_consumers;
    Buffer::Shared m_header;
    bool m_streaming;
};

#endif