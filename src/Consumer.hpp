/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:11:54
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef CONSUMER_HPP__
#define CONSUMER_HPP__

#include <condition_variable>
#include <deque>
#include <memory>
#include <mutex>

#include <ccut/Queue.hpp>

#include "Buffer.hpp"

class Streamer;

class Consumer : public std::enable_shared_from_this<Consumer>
{
public:
    typedef std::shared_ptr<Consumer> Shared;
    typedef size_t Id;

    Consumer() noexcept;
    virtual ~Consumer();

    const Id id;

    virtual void initialize(size_t maxPackets);

    enum State
    {
        NOT_INIT,
        WAIT_HEADER,
        WAIT_SYNC,
        STREAMING
    };

    /**
     * @brief enqueue a buffer
     * @details
     * - depending on state buffers may not be queued
     * - on overflow queue is flushed and state moved to WAIT_SYNC
     * - state is updated by this method
     * @param packet
     * @return true if packet was enqueued
     */
    virtual bool push(Buffer::Shared &packet);

    void subscribe(std::shared_ptr<Streamer> &streamer);
    void unsubscribe();

protected:
    /**
     * @brief internal enqueuing function
     * @details used by `push`
     * @param packet
     * @return true if packet was enqueued
     */
    bool enqueue(Buffer::Shared &packet);

    State m_state = NOT_INIT;
    std::unique_ptr<ccut::Queue<Buffer::Shared>> m_packets = nullptr;
    std::weak_ptr<Streamer> m_streamer;
};

#endif