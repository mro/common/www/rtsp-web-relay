/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-23T21:39:08
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "StreamerFactory.hpp"

#include <chrono>

#include <ccut/yml.hpp>
#include <logger/Logger.hpp>

namespace yml = ccut::yml;

const std::string s_logCat{"rtsp:streamer"};

StreamerFactory::~StreamerFactory()
{
    std::lock_guard<std::mutex> lock(m_lock);
    for (auto it : m_streamers)
        it.second->stop();
}

void StreamerFactory::load(const std::string &config)
{
    try
    {
        yml::Parser parser;
        yml::Tree tree = parser.parse_in_arena("<yml>", yml::to_csubstr(config));
        yml::NodeRef groups = yml::get(tree.rootref(), "groups");
        yml::Tree publicConfig;

        uint64_t unsubscribeTimeout;
        yml::get(tree.rootref(), "unsubscribeTimeout") >>
            yml::default_to(
                unsubscribeTimeout,
                uint64_t(Streamer::defaultUnsubscribeTimeout.count()));

        {
            std::lock_guard<std::mutex> lock(m_lock);
            m_streamers.clear();
        }
        publicConfig.rootref() |= yml::MAP;

        if (!groups.valid() || !groups.is_map())
            throw std::runtime_error("no groups tag");
        for (const yml::NodeRef &group : groups)
        {
            std::string groupName;
            group >> yml::default_to(yml::key(groupName), std::string());
            if (!group.is_map())
                throw std::runtime_error("group element is not an object");

            yml::NodeRef groupDesc =
                publicConfig.rootref()[publicConfig.copy_to_arena(
                    yml::to_csubstr(groupName))];
            groupDesc |= yml::MAP;
            yml::NodeRef camerasDesc = groupDesc["cameras"];
            camerasDesc |= yml::SEQ;

            if (group.has_child("description"))
                groupDesc["description"] = group["description"].val();

            yml::NodeRef cameras = yml::get(group, "cameras");
            if (!cameras.valid() || !cameras.is_seq())
                throw std::runtime_error("cameras element is not an array");
            for (const yml::NodeRef &camera : cameras)
            {
                std::string name;
                std::string url;
                yml::get(camera, "name") >> yml::default_to(name, std::string());
                yml::get(camera, "url") >> yml::default_to(url, std::string());

                yml::NodeRef cameraDesc = camerasDesc.append_child();
                cameraDesc |= yml::MAP;
                cameraDesc["name"] << name;
                if (camera.has_child("extra"))
                {
                    for (const yml::NodeRef &extraInfo : camera["extra"])
                        cameraDesc[extraInfo.key()] = extraInfo.val();
                }
                if (camera.has_child("description"))
                    cameraDesc["description"] << camera["description"].val();

                if (name.empty())
                    throw std::runtime_error("camera element has no name");
                else if (url.empty())
                    throw std::runtime_error("camera element has no url");

                logger::info(s_logCat) << "loading camera: " << groupName << "/"
                                       << name << " source: " << url;
                std::lock_guard<std::mutex> lock(m_lock);
                m_streamers[groupName + "/" + name] = std::make_shared<Streamer>(
                    url, std::chrono::milliseconds{unsubscribeTimeout * 1000});
            }
        }

        {
            std::lock_guard<std::mutex> lock(m_lock);
            m_config = yml::emitrs_json<std::string>(publicConfig);
        }
    }
    catch (std::exception &ex)
    {
        logger::error(s_logCat) << "failed to parse config: " << ex.what();
    }
}
