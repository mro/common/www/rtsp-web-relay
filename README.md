# RTSP/Web relay

This project is an RTSP to (f)mp4 streamer.

It relies on ffmpeg libraries for the video decoding/encoding, and provides
advanced multiplexing and buffering features to stream videos in web-browsers.

This project does the following:
- Connect an RTSP source containing H.264 stream.
- Embed the H.264 stream in an fragmented mp4 container (f)mp4.
- Serve the (f)mp4 stream to HTTP clients.

This implementation:
- Connects each RTSP source only once
- Streams the same content to all clients, forwarding content as it arrives, properly syncing newcomers. 
- Uses a pre-allocated fixed-size pool of buffers.
- Drops and tries to re-sync slow clients (not impacting others).

## Build

To build this project it is recommended to install [docker-builder](https://gitlab.cern.ch/mro/common/tools/x-builder) script.

To build the application:
```bash
x-builder

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Testing

To run unit-tests:
```bash
x-builder ./build/tests/test_all
```

To run linters:
```bash
x-builder make -C build lint
```

## Debugging

To compile this component for gdb/lldb:
```bash
x-builder

cd build && cmake3 -DCMAKE_BUILD_TYPE=Debug ..
```
