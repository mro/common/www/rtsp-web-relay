/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-10T13:27:09+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "helpers_local.hpp"

#include <sstream>

CPPUNIT_NS_BEGIN

    CPPUNIT_TRAITS_OVERRIDE(std::vector<std::int32_t>)
    {
        std::ostringstream oss;

        oss << "std::vector({";
        bool first = true;
        for (std::int32_t i : t)
        {
            if (first)
                oss << i;
            else
                oss << ", " << i;
            first = false;
        }
        oss << "})";
        return oss.str();
    }

CPPUNIT_NS_END
