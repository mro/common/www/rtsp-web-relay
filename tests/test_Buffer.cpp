/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-04-20T00:12:46
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Buffer.hpp"
#include "helpers_local.hpp"

class TestBuffer : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestBuffer);
    CPPUNIT_TEST(append);
    CPPUNIT_TEST_SUITE_END();

public:
    void append()
    {
        Buffer buffer(127);
        std::string txt{"test"};

        buffer.append(txt.begin(), txt.end());
        EQ(size_t{4}, buffer.size());

        buffer.append(&txt[0], &txt[4]);
        EQ(size_t{8}, buffer.size());

        EQ(std::string("testtest"), std::string(buffer.begin(), buffer.end()));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBuffer);
