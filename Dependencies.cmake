
# EDITME: add your find_package(...) calls here
find_package(PkgConfig REQUIRED)

pkg_check_modules(avcodec REQUIRED IMPORTED_TARGET "libavcodec >= 58")
pkg_check_modules(avutil REQUIRED IMPORTED_TARGET "libavutil >= 56")
pkg_check_modules(avformat REQUIRED IMPORTED_TARGET "libavformat >= 58")

add_library(ffmpeg INTERFACE IMPORTED)
target_link_libraries(ffmpeg
    INTERFACE PkgConfig::avcodec PkgConfig::avformat PkgConfig::avutil)

find_package(CCUT REQUIRED)
find_package(OATPP REQUIRED)